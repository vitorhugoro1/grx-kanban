<?php get_header( ); ?>
<main>
  <?php if (have_posts()) : ?>

  	<?php while (have_posts()) : the_post(); ?>

        <h1><?php the_title(); ?></h1>
        
        <form method="post" id="novo-projeto" action="<?php echo get_template_directory_uri(); ?>/actions/novo-projeto.php" >
          <label for="nome-projeto">Nome do projeto <span style="color:red;">*</span></label>
          <input type="text" class="u-full-width" name="nome-projeto" id="nome-projeto" placeholder="Nome do projeto" maxlength="20" required />
          <legend><span style="color:red;">*</span> <small>Tamanho maxímo de 20 caracteres.</small> </legend>
          <input type="hidden" id="url" value="<?php echo get_template_directory_uri();?>/actions/cadastrar-usuario.php">
          <input type="submit" value="<?php echo __('Save'); ?>">
        </form>

  	<?php endwhile; ?>

  		<?php // Navigation ?>

  	<?php else : ?>

  		<?php // No Posts Found ?>

  <?php endif; ?>
</main>
<?php get_footer( ); ?>
