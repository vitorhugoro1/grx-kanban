<?php
error_reporting(1);
if(!is_user_logged_in()){ header('Location:'.home_url());}
if(empty($_GET['projeto']) || $_GET['projeto'] == ''){header('Location:'.home_url());}

$projetos_front = projetos_front();
$projetoStatus = in_array($_GET['projeto'], $projetos_front['inativo']);
$taxonomies = 'tarefas_'.$_GET['projeto'];
$terms = get_terms( $taxonomies, array('hide_empty' => false) );

$users = get_users(array('exclude'  => array(wp_get_current_user()->ID)));
?>
<?php get_header(); ?>
<main>
  <?php if (have_posts()) : ?>

  	<?php while (have_posts()) : the_post(); ?>
  		  <div class="row">
            <h3><?php the_title(); ?> - <?php echo strtoupper($_GET['projeto']); ?></h3>
  		  </div>
        <div class="row">
            <form method="post" action="<?php echo get_template_directory_uri(); ?>/actions/edit-projeto.php" id="editar-projeto">
              <div class="row">
                <legend>Colunas</legend>
                <table class="columns-fields u-full-width">
                  <?php if(!empty($terms)): ?>
                    <thead>
                      <tr>
                        <th>
                          Nome da coluna
                        </th>
                        <th>
                          Ordem
                        </th>
                        <th>
                          Excluir?
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $sizeOf = count($terms); ?>
                    <?php foreach ($terms as $term): ?>
                      <?php $term_order = get_term_meta($term->term_id, 'ordem'); ?>
                            <tr>
                              <th scope="row">
                                <label for="<?php echo $term->term_id; ?>"><em><?php echo $term->name; ?></em></label>
                              </th>
                              <td>
                                <select name="ordem-<?php echo $term->slug; ?>">
                                  <?php
                                    for($i = 1; $i < $sizeOf + 1; $i++){
                                      ?>
                                        <option value="<?php echo $i;?>" <?php echo ($i == $term_order[0]) ? 'selected' : '';?>><?php echo $i;?></option>
                                      <?php
                                    }
                                   ?>
                                </select>
                              </td>
                              <td>
                                <input type="checkbox" name="terms[]" id="<?php echo $term->term_id; ?>" value="<?php echo $term->term_id; ?>"/> Excluir?
                              </td>
                            </tr>
                    <?php endforeach; ?>
                  </tbody>
                  <?php else: ?>
                    <b>Não há colunas criadas.</b>
                  <?php endif; ?>
                </table>
                <div class="row term-fields">
                  <input type="text" class="four columns u-cf" name="terms_new[]" value="" />
                </div>
                <a href="#" id="add-term-button" class="button">Adicionar coluna</a>
                <a href="#" id="exclude-term-button" class="button button-warning" style="display:none;">Excluir coluna</a>
              </div>
              <div class="row">
                <legend>Membros</legend>
                <ul>
                <?php if(!empty($terms)): ?>
                  <?php foreach ($users as $user): ?>
                      <?php $projetos = get_the_author_meta('projetos', $user->ID);
                        $projetos = unserialize($projetos);
                       ?>
                       <li>
                         <label for="<?php echo $user->ID; ?>">
                           <?php
                           if(!$projetos || !in_array($_GET['projeto'], $projetos)) :
                           ?>
                           <input type="checkbox" name="membros[]" id="<?php echo $user->ID; ?>" value="<?php echo $user->ID; ?>"  /> <?php echo get_the_author_meta('display_name', $user->ID); ?>
                         <?php else: ?>
                           <?php echo get_the_author_meta('display_name', $user->ID); ?>
                           <input type="checkbox" name="membros_rem[]" id="<?php echo $user->ID; ?>" value="<?php echo $user->ID; ?>" /> Excluir?
                         <?php endif; ?>
                         </label>
                       </li>
                  <?php endforeach; ?>
                <?php else: ?>
                  <b>Não há membros adicionais ao projeto.</b>
                <?php endif; ?>
                </ul>
              </div>
              <div class="row">
                <legend>Alterar Status do Projeto</legend>
                <?php var_dump(  ); ?>
                <select name="status">
                  <option value="1" <?php echo (!$projetoStatus) ? 'selected' : ''; ?>>Ativo</option>
                  <option value="0" <?php echo ($projetoStatus) ? 'selected' : ''; ?>>Inativo</option>
                </select>
              </div>
              <input type="hidden" name="post_type" value="<?php echo $_GET['projeto']; ?>"/>
              <input type="submit" class="button-primary" value="<?php _e('Edit'); ?>"/>
            </form>
        </div>
  	<?php endwhile; ?>
    <?php endif; ?>
</main>
<?php get_footer(); ?>
