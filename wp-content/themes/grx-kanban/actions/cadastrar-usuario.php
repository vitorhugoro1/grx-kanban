<?php
require '../../../../wp-blog-header.php';
header("Access-Control-Allow-Origin: *");

if(!email_exists( $_POST['email'] ) && !username_exists( $_POST['email'] )){
  $userdata = array(
    'user_login'    => $_POST['email'],
    'user_pass'     => $_POST['senha'],
    'display_name'  => $_POST['nome'],
    'user_email'    => $_POST['email'],
    'nickname'      => sanitize_title( $_POST['nickname'] ),
    'role'          => 'editor'
  );

  $user_id = wp_insert_user( $userdata );

  if(!is_wp_error( $user_id )){
    $projetos = array();
    update_user_meta( $user_id, 'funcao', esc_attr( $_POST['funcao'] ) );
    update_user_meta( $user_id, 'projetos', serialize($projetos) );
    update_user_option($user_id, 'show_admin_bar_front', false);

    wp_insert_term( $_POST['nome'] , 'team', array(
      'slug'  => sanitize_title( $_POST['nickname'] )
    ) );

    echo 'Usuário cadastrado com sucesso';
  } else {
    echo 'Erro ao cadastrar usuário, comunique o administrador';
  }
} else {
  echo 'email';
}

if(wp_get_referer()){
  wp_safe_redirect( get_home_url(null, '/login') );
} else {
  wp_safe_redirect( get_home_url(null, '/login') );
}

?>
