<?php
require '../../../../wp-blog-header.php';

$message = array(
  'status'  => '',
  'message' => ''
);


/**
 * Verifica se há projetos cadastrados no opt-post_type
 * e carrega os status
 */

if(get_option('opt-post_type') !== '') {
    $options = unserialize(get_option('opt-post_type'));
    if(get_option('opt-status_projeto') !== ''){
        $status = unserialize(get_option('opt-status_projeto'));
    } else {
        $status = array();
    }
} else {
    $options = array();
    if(get_option('opt-status_projeto') !== ''){
        $status = unserialize(get_option('opt-status_projeto'));
    } else {
        $status = array();
    }
}

/**
 * Verifica se o projeto se está cadastrado
 * Se FALSE, ele cadastra na opt-post_type
 * Se TRUE, ele envia mensagem de error
 */

if(!in_array($_POST['nome-projeto'],$options)){
    $slug = sanitize_title($_POST['nome-projeto']);
    $status[$slug] = '1';
    array_push($options, $_POST['nome-projeto']);
    update_option('opt-post_type', serialize($options));
    update_option('opt-status_projeto', serialize($status));

    $projetos = get_the_author_meta(wp_get_current_user()->ID, 'projetos');
    $projetos = is_serialized($projetos) ? unserialize($projetos) : $projetos;

    if($projetos == '' || $projetos === FALSE || !is_array($projetos)){
      $new = array($slug);
      $new = serialize($new);
      echo $new;
      update_user_meta(wp_get_current_user()->ID, 'projetos', $new);
    } else {
      if(!in_array($slug, $projetos)){
        $new = array_push($projetos, $slug);
        $new = serialize($new);
        update_user_meta(wp_get_current_user()->ID, 'projetos', $new);
      }
    }
    $message['status'] = 'success';
    $message['message'] = 'Projeto cadastrado com sucesso';
} else {
    $message['status'] = 'error';
    $message['message'] = 'Projeto já criado, verifique mais com no seu painel de administração';
}

echo json_encode( $message );

if(wp_get_referer()){
  wp_safe_redirect(  get_home_url( ) );
} else {
  wp_safe_redirect( get_home_url( ) );
}
