<?php
require '../../../../wp-blog-header.php';

$membros_rem = $_POST['membros_rem'];
$membros_add = $_POST['membros'];
$terms_ex = $_POST['terms'];
$terms = $_POST['terms_new'];
$taxonomy = 'tarefas_'.$_POST['post_type'];
$post_type = $_POST['post_type'];

if(isset($_POST['status'])){
  $status = unserialize(get_option('opt-status_projeto'));
  $status[$post_type] = $_POST['status'];
  update_option('opt-status_projeto', serialize($status));
}

foreach(get_terms($taxonomy, array('hide_empty' => false)) as $term){
  $order = get_term_meta($term->term_id, 'ordem')[0];
  if(isset($_POST['ordem-'.$term->slug]) && $_POST['ordem-'.$term->slug] !== $order ){
    update_term_meta($term->term_id, 'ordem', intval($_POST['ordem-'.$term->slug]));
  }
}

if($terms[0] !== ''){
  foreach($terms as $term){
    if(!has_term( $term, $taxonomy)){
      $validation = wp_insert_term( $term, $taxonomy, array('slug' => sanitize_title($term)) );
      if(is_wp_error( $validation )){
        echo 'erro';
      } else {
        $c = intval(count(get_terms($taxonomy, array('hide_empty' => false)))) + 1;
        update_term_meta($validation['term_id'], 'ordem', $c);
        echo 'sucesso';
      }
    } else {
      echo 'Coluna já existe';
      break;
    }
  }
}

if(isset($_POST['terms'])){
  foreach($terms_ex as $term_id){
    $validation = wp_delete_term($term_id, $taxonomy);
    if(is_wp_error( $validation )){
      echo 'erro';
    } else {
      echo 'sucesso';
    }
  }
}

if(isset($_POST['membros'])){
  foreach($membros_add as $user_id){
    $projetos = get_the_author_meta('projetos', $user_id);
    $projetos = is_serialized($projetos) ? unserialize($projetos) : $projetos;

    if($projetos == '' || $projetos === FALSE || !is_array($projetos)){
      $new = array($post_type);
      $new = serialize($new);
      echo $new;
      update_user_meta($user_id, 'projetos', $new);
    } else {
      if(!in_array($post_type, $projetos)){
        $new = array_push($projetos, $post_type);
        $new = serialize($new);
        update_user_meta($user_id, 'projetos', $new);
      }
    }
  }
}

if(isset($_POST['membros_rem'])){
  foreach($membros_rem as $user_id){
    $projetos = get_the_author_meta('projetos', $user_id);
    $projetos = is_serialized($projetos) ? unserialize($projetos) : $projetos;

    $key = array_search($post_type, $projetos);
    if($key !== false){
      unset($projetos[$key]);
    }

    $new = serialize($projetos);

    update_user_meta($user_id, 'projetos', $new);
  }
}

if(wp_get_referer()){
  wp_safe_redirect( wp_get_referer() );
} else {
  wp_safe_redirect( get_home_url() );
}
