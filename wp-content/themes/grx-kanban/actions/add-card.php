<?php
require '../../../../wp-blog-header.php';

extract($_POST);
$message = array(
                  'status'  => '',
                  'message' => ''
                );
$user_id = get_current_user_id();

$tarefa_id = register_new_card($add_name, '', '', $slug, $user_id);

$team = wp_set_object_terms( $tarefa_id, get_the_author_meta('nickname', $user_id), 'team');
$object_term = wp_set_object_terms( $tarefa_id, array(intval($term_id)), $taxonomy );

if($tarefa_id || !is_wp_error( $object_term ) || !is_wp_error( $team )){
  $message['status'] = 'success';
  $message['message'] = 'Sucesso ao cadastrar novo card';

  echo json_encode( $message );
  if(wp_get_referer()){
    wp_safe_redirect( wp_get_referer() );
  } else {
    wp_safe_redirect( get_home_url() );
  }
} else {
  $message['status'] = 'error';
  $message['message'] = 'Erro ao cadastrar novo card';

  echo json_encode( $message );
}
