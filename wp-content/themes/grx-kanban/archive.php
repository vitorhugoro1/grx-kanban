<?php get_header(); ?>
<?php $slug = get_queried_object()->rewrite['slug'];?>
<main>
  <div class="row">
    <h1> <?php post_type_archive_title('Projeto - '); ?>  <a href="<?php echo get_permalink_by('title', 'Editar projeto'); ?>?projeto=<?php echo get_queried_object()->rewrite['slug']; ?>" class="button">Editar</a> </h1>
  </div>
  <div class="colunas-items u-max-full-width">
    <div class="colunas">
      <?php
      $projetos_front = projetos_front();
      $projetoStatus = in_array($_GET['projeto'], $projetos_front['inativo']);
      $taxonomies = 'tarefas_'.$slug;

      $testes = get_terms($taxonomies, array('hide_empty' => false));
      $colunas = array(
        1 => 'Pré Game',
        2 => 'Sprint',
        3 => 'Fazendo',
        4 => 'Concluído',
        5 => 'Impedimento',
        6 => 'Arquivo'
      );
      foreach($testes as $term){
        if(!empty(get_term_meta($term->term_id, 'ordem'))){ break;}

        foreach($colunas as $key => $value){
          if($term->name == $value){
            update_term_meta($term->term_id, 'ordem', intval($key));
          }
        }
      }


      $terms = get_terms( $taxonomies, array('hide_empty' => false, 'orderby' => 'meta_value_num', 'meta_key' => 'ordem') );
      foreach($terms as $term_info){
        query_posts( $query_string . '&'.$taxonomies.'='.$term_info->slug );
        ?>
          <div id="<?php echo $term_info->term_id; ?>" class="tarefa" data-posttype="<?php echo $slug;?>">
            <h3><strong><?php echo $term_info->name; ?></strong> <span class="peso-total"></span></h3>
            <ul id="lista_tarefas_<?php echo $term_info->term_id; ?>">
              <?php
              if (have_posts()) :
                while (have_posts()) : the_post(); ?>
                <?php $membros = unserialize(get_post_meta(get_the_id(), 'membros', true)); ?>
                  <li data-postid="<?php echo the_ID(); ?>" class="box-draggable" >
                    <a href="#" class="open-modal" data-posttype="<?php echo get_post_type();?>" data-postid="<?php echo the_ID(); ?>"><?php the_title( $before = '', $after = '', $echo = true ) ?></a>
                    <div class="total-membros" data-postid="<?php echo the_ID(); ?>"></div>
                    <div class="tags"></div>
                  </li>
                <?php endwhile; wp_reset_query();
              else : ?>
                <span class="sem-card"><strong><?php echo __('Sem cards'); ?></strong></span>
              <?php
              endif;
              ?>
            </ul>
            <?php if(is_null($projetoStatus) || empty($projetoStatus)) : ?>
            <div class="row add-card">
              <form id="add-card" method="post" action="<?php echo home_url();?>/wp-content/themes/grx-kanban/actions/add-card.php">
                <?php wp_referer_field( $echo ) ?>
                <input type="hidden" name="term_id" value="<?php echo $term_info->term_id; ?>">
                <input type="hidden" name="taxonomy" value="<?php echo $taxonomies; ?>">
                <input type="hidden" name="slug" value="<?php echo $slug; ?>"/>
                <input type="hidden" name="user_id" id="user_id" value="<?php echo get_current_user_id(); ?>">
                <input type="text" name="add_name" id="add_name" placeholder="Titulo do Card" required/>
                <input type="submit" id="submit-add-card" class="button u-pull-left" value="Adicionar">
                <a href="#" class="close-add-card">X</a>
              </form>
            </div>
            <a href="#" class="button add-card-actived" >Adicionar card</a>
          <?php endif; ?>
          </div>
        <?php
       }
         ?>
    </div>

  </div>
  <div class="modal">
    <div class="modal-content">
      <div class="row title-editable">
        <span class="close">x</span>
        <h4 contenteditable="false" class="m-title"></h4>
        <!-- <span class="lista-atual">Na lista </span> -->
        <a href="#" class="edit-content">Editar</a>
        <label for="esforco">Esforço</label>
        <select class="peso-tarefa js-peso-tarefa" id="esforco">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="5">5</option>
          <option value="8">8</option>
          <option value="13">13</option>
          <option value="21">21</option>
        </select>
        <label for="esforco_realizado">Esforço Realizado</label>
        <select class="peso-tarefa js-peso-tarefa" id="esforco_realizado">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="5">5</option>
          <option value="8">8</option>
          <option value="13">13</option>
          <option value="21">21</option>
        </select>
        <div class="u-cf"></div>
        <a href="#" class="save-editable button">Salvar</a>
        <a href="#" class="cancel-editable button button-warning">Cancelar</a>
      </div>
      <div class="row">
        <span class="modal-title">Membros</span>
        <div class="u-cf"></div>
        <span>Membros existentes</span>
        <ul class="membros-atual"></ul>
        <div class="u-cf"></div>
        <span>Membros para adicionar</span><br>
        <select name="membro_add" class="membros-list">
        </select>
        <a href="#" class="button add-membro-active button-primary">Adicionar membro</a>
      </div>
      <div class="row">
        <span class="modal-title">Etiquetas</span>
        <div class="u-cf"></div>
        <ul class="tags-list"></ul>
        <select class="tags_color" name="tag-list">
        </select>
        <a class="button button-primary add-tag">Adicionar etiqueta</a>
      </div>
      <div class="row content-editable">
        <span class="modal-title">Descrição</span>
        <a href="#" class="edit-content">Editar</a>
        <p class="m-content" contenteditable="false"></p>
        <div class="u-cf"></div>
        <a href="#" class="save-editable button">Salvar</a>
        <a href="#" class="cancel-editable button button-warning">Cancelar</a>
      </div>
      <div class="row comment-form">
        <form method="post">
          <legend>Adicionar comentário</legend>
          <textarea name="comment-content" class="u-full-width"></textarea>
          <input type="submit" class="button-primary" value="Enviar"/>
        </form>
      </div>
      <div class="row comment-fields">
        <span class="modal-title">Atividade</span>
        <ul class="comment-list">
        </ul>
        <span class="comment-message"></span>
        <div class="load-pik">
          <img src="<?php echo get_template_directory_uri().'/inc/img/loader-pik.gif';?>" alt="" />
        </div>
      </div>
    </div>
  </div>

</main>
<?php get_footer(); ?>
