<?php

require 'extensions/custom-post-meta.php';
require 'extensions/custom-rest-api.php';

/**
 * Carrega o gerador de post-types.
 */
require 'plugins/post-type/init.php';

/*
 * Carrega o plugin de Custom Meta Boxes
 */
//
// if (file_exists(__DIR__.'/plugins/cmb2/init.php')) {
//     require_once '/plugins/cmb2/init.php';
// } elseif (file_exists(__DIR__.'/plugins/CMB2/init.php')) {
//     require_once __DIR__.'/plugins/CMB2/init.php';
// }

/**
 * Carrega os arquivos css e js (javascript) ao wp_head.
 */
function add_stylesheet()
{
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_script('jquery' , get_template_directory_uri().'/inc/js/jquery-3.1.0.min.js', array('jquery'), '3.1.0', true );
    wp_enqueue_script('jqueryui' , get_template_directory_uri().'/inc/js/jquery-ui.min.js', array('jquery'), '1.12.0', true );
    wp_enqueue_script('bootstrap' , get_template_directory_uri().'/inc/js/bootstrap.min.js', array('jquery'), '3.3.7', true );
    wp_localize_script( 'wp-api', 'wpApiSettings', array( 'root' => esc_url_raw( rest_url() ), 'nonce' => wp_create_nonce( 'wp_rest' ) ) );
    wp_enqueue_script('wp-api');
    wp_enqueue_script('main' , get_template_directory_uri().'/inc/js/main.js', array('jquery'), '1.0.0', true );
    wp_enqueue_style('skeleton', get_template_directory_uri().'/inc/css/skeleton.css');
    wp_enqueue_style('normalize', get_template_directory_uri().'/inc/css/normalize.css');
}

add_action('wp_enqueue_scripts', 'add_stylesheet');

/**
 * @return array
 */
function post_type_list()
{
    $arr = array();
    $post_types = unserialize(get_option('opt-post_type'));

    if(!empty($post_types)){
      foreach ($post_types as $post_type) {
          $arr[] = sanitize_title($post_type);
      }
    }

    return $arr;
}

function update_cmb2_meta_box_url($url)
{
    /*
     * If you use a symlink, the css/js urls may have an odd path stuck in the middle, like:
     * http://SITEURL/wp-content/plugins/Users/jt/Sites/CMB2/cmb2/js/cmb2.js?ver=X.X.X
     * Or something like that.
     *
     * INSTEAD of completely replacing the URL,
     * It is best to do a str_replace. This ensures you only change the url if it's
     * pointing to the broken resource. This ensures that if another version of CMB2
     * is loaded (i.e. in a 3rd part plugin), that their correct URL will load,
     * rather than forcing yours.
     */

  //  return str_replace('C:/xampp/htdocs/grx-kanban', '', $url);
}
add_filter('cmb2_meta_box_url', 'update_cmb2_meta_box_url');

/**
 * Gera uma nova página a partir da passagem dos paramêtros
 *
 * @param string $new_page_title    Título da nova página
 * @param string $new_page_content  Conteúdo da nova página
 * @param string $new_page_template Nome do arquivo ou template, que a pagina tem
 *
 * @return int|boolean Retorna o id da nova página se gerada, se erro retorna FALSE
 */
function register_new_page($new_page_title, $new_page_content = null, $new_page_template = null)
{
    $page_check = get_page_by_title($new_page_title);
    $new_page = array(
            'post_type' => 'page',
            'post_title' => $new_page_title,
            'post_content' => $new_page_content,
            'post_status' => 'publish',
            'post_author' => 1,
    );
    if (!isset($page_check->ID)) {
        $new_page_id = wp_insert_post($new_page);
        if (!empty($new_page_template)) {
            update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
        }
    }

    return $new_page_id;
}

function register_new_card($new_page_title, $new_page_content = null, $new_page_template = null, $slug, $user_id)
{
    $page_check = get_page_by_title($new_page_title);
    $new_page = array(
            'post_type' => $slug,
            'post_title' => $new_page_title,
            'post_content' => $new_page_content,
            'post_status' => 'publish',
            'post_author' => $user_id,
            'comment_status' => 'open'
    );

    if (!isset($page_check->ID)) {
        $new_page_id = wp_insert_post($new_page);
        if (!empty($new_page_template)) {
            $membros = array($user_id);
            update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
            update_post_meta($new_page_id, 'membros', serialize($user_id));
        }
    }

    return $new_page_id;
}

/**
 * Gera as páginas padrões para o funcionamento do tema
 *
 */

add_action('init', 'paginas_padrao');

function paginas_padrao(){
    register_new_page('Cadastrar');
    register_new_page('Login');
    register_new_page('Perfil');
    register_new_page('Novo projeto');
    register_new_page('Editar projeto');
}


/**
 * Gerar o permalink de uma página, a partir de um paramêtro (nome/name/slug/path), e um post_type especifico
 * @param  string $parameter Qual a forma que vai pesquisar a pagina
 * @param  string $value     Valor que vai pesquisar
 * @param  string $post_type Pegar página de um post type especifico
 * @return string|boolean Retorna o link da página se achada, ou FALSE
 */

function get_permalink_by($parameter, $value, $post_type = 'page'){
  switch($parameter){
    case 'name':
    case 'nome':
    case 'title':
      $page = get_page_by_title( $value, $output, $post_type);
      $page = $page->ID;
    case 'path':
    case 'slug':
      $page = get_page_by_path( $value, $output, $post_type);
      $page = $page->ID;
  }

  return esc_url( get_permalink($page) );
}

add_action('init', 'register_team');

function register_team(){
  $post_types = post_type_list();
  $singular_label = 'time';

  $labels = array(
      'name'              => __( 'Time'),
      'singular_name'     => __( $singular_label),
      'search_items'      => __( 'Procurar '.$singular_label ),
      'all_items'         => __( 'All '.$singular_label ),
      'parent_item'       => __( 'Parent '.$singular_label ),
      'parent_item_colon' => __( 'Parent '.$singular_label.':' ),
      'edit_item'         => __( 'Editar '.$singular_label ),
      'update_item'       => __( 'Atualizar '.$singular_label ),
      'add_new_item'      => __( 'Adicionar nova '.$singular_label ),
      'new_item_name'     => __( 'Nova '.$singular_label ),
      'menu_name'         => __('Time'),
  );

  $args = array(
      'hierarchical'      => true,
      'labels'            => $labels,
      'show_ui'           => false,
      'show_admin_column' => false,
      'query_var'         => true,
      'show_in_rest'      => true,
      'rest_base'         => 'team_api',
      'rest_controller_class' => 'WP_REST_Terms_Controller'
    );

  register_taxonomy('team', $post_types, $args);
}

add_action('init', 'tags_colors');

function tags_colors(){
  $tags = get_terms('post_tag', array('hide_empty' => false));
  if(count($tags) == 0){
    $colunas = array(
      1 => array(
        'nome'  => 'Backend',
        'color' => '#2196F3'
      ),
      2 => array(
        'nome'  => 'Frontend',
        'color' => '#f44336'
      ),
      3 => array(
        'nome'  => 'Administrativo',
        'color' => '#FFEB3B'
      ),
      4 => array(
        'nome'  => 'Laranja',
        'color' => '#FF9800'
      ),
      5 => array(
        'nome'  => 'Roxo',
        'color' => '#673AB7'
      ),
      6 => array(
        'nome'  => 'Cinza',
        'color' => '#9E9E9E'
      )
    );

    foreach($colunas as $key => $value){
      $validation = wp_insert_term( $value['nome'], 'post_tag', array('slug' => sanitize_title($value['nome']), 'description' => $value['color']) );

      if(is_wp_error( $validation )){
        die(is_wp_error( $validation ));
      }
    }
  }
}

add_action('profile_update', 'profile_update_modify');

function profile_update_modify($user_id, $old_user_data){
  $term = get_term_by( 'name', $old_user_data->nickname, 'team');
  $user = get_userdata( $user_id );

  if( $term ){
    $validation = wp_update_term($term->term_id, 'team', array(
      'name'  => get_the_author_meta( 'display_name', $user_id ),
      'slug'  => get_the_author_meta('nickname', $user_id)
    ));
  } else {
    $validation = wp_insert_term( $user->display_name, 'team', array(
      'slug'  => sanitize_title($user->nickname)
    ));
  }
  if(is_wp_error( $validation )){
    die($validation);
  }
}
