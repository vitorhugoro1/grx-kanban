// cache team_api
var team = [];
var tags = [];

// cache user_id
var userid = parseInt(jQuery('#userid').val());

var modal = jQuery('.modal');
var content = modal.children('.modal-content').children('.row');

//cache div com todas as colunas
var node = "";

//organiza o box das colunas
function organizaColunas($) {

  node = $('.colunas');
  var len = node.children().length;
  var wColuna = 200;
  var wRespiro = 16;

  var totalColunas = (200 * len) + ((len - 1) * wRespiro);
  //console.log(len, (200 * len),((len-1) *wRespiro), "width",(totalColunas+"px"));
  node.css({
    "width": (totalColunas + "px")
  });
}


//DRAG N' DROP

function adicionaListenersDragNDrop($) {
  node.children().each(function(idx, element) {
    //console.log(idx,element);
    $(element).droppable({
      drop: function(event, ui) {

        //id da li
        var liid = "#" + event.originalEvent.target.getAttribute("data-postid");
        //id da lista
        var ulid = "#lista_tarefas_" + event.target.id;
        // post id
        var postId = event.originalEvent.target.getAttribute("data-postid");

        //loop por todos os cards
        $('.box-draggable').each(function() {
          //achou o card movido
          if ($(this).data("postid") == event.originalEvent.target.getAttribute("data-postid")) {
            //move o card  para a lista correta
            $(this).prependTo(ulid);
            return false;
            //console.log($(this));
          }
        })

        $(ulid).children().each(function(idx, elemnt) {
          console.log(elemnt.className);
          if (elemnt.className == "sem-card") {
            //esconde o sem cards
            $(elemnt).hide();
          }
        });
        // chama servico pra salvar
        var termId = parseInt($(ulid).parent().attr('id'));
        var taxonomy = 'tax-' + $(ulid).parent().find('input[name^="taxonomy"]').val();
        var postType = $(ulid).parent().find('input[name^="slug"]').val();
        var dataPOST = {};
        dataPOST[taxonomy] = [termId];

        console.log(dataPOST);
        $.ajax({
          url: wpApiSettings.root + 'wp/v2/' + postType + '-api/' + postId,
          method: 'POST',
          beforeSend: function(xhr) {
            xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
          },
          data: dataPOST
        }).done(function(response) {
          // console.log( response );
          pesoTarefas($);
        });

      }
    });
  });
}



function adicionaDrag($) {
  //console.log($('.box-draggable'));

  $('.box-draggable').draggable({
    cursor: 'move',
    helper: "clone"
  });
}

function pesoTarefas($) {
  var colunas = $(".colunas");

  // valida a quantidade de colunas
  if(colunas.length == 0){ return; }

  jQuery('#overlay').appendTo('body').show();

  //  Percorre todas as colunas
  $.each(colunas.children(), function(idx, value) {
    var esforco = 0;
    // Carrega as informações da postagem
    $.getJSON(wpApiSettings.root + 'wp/v2/' + value.dataset.posttype + '-api?tax-tarefas_' + value.dataset.posttype + '=' + value.id).
    done(function(response) {
      if (response.length > 0) {
        $.each(response, function(id, data) {
          colunas.children("#" + value.id)
          .children('ul')
          .children('[data-postid="' + data.id + '"]')
          .children('.tags').html("");

          if (data.team_api.length > 1) {
            colunas.children("#" + value.id)
              .children("ul")
              .children('[data-postid="' + data.id + '"]')
              .children('.total-membros').html(data.team_api.length + " membros");
          } else if (data.team_api.length == 0) {
            colunas.children("#" + value.id)
              .children("ul")
              .children('[data-postid="' + data.id + '"]')
              .children('.total-membros').html("Sem membro");
          } else {
            colunas.children("#" + value.id)
              .children("ul")
              .children('[data-postid="' + data.id + '"]')
              .children('.total-membros').html(data.team_api.length + " membro");
          }

          if(data.tags.length > 0){
            jQuery.each(data.tags, function(idt, vlx){
              jQuery.ajax({
                url: wpApiSettings.root + 'wp/v2/tags/' + vlx,
                type: 'get',
                success: function(rpx){
                  element = '<div class="tags-cloud" style="background-color:' + rpx.description + '"></div>';
                  colunas.children("#" + value.id)
                  .children('ul')
                  .children('[data-postid="' + data.id + '"]')
                  .children('.tags').append(element);
                }
              });
            });
          }

          if (data.esforco == 0) {
            data.esforco = 1;
          }
          esforco += data.esforco;
        });

        colunas.children("#" + value.id).children('h3').children('.peso-total').html(esforco);
      } else {
        colunas.children("#" + value.id).children('h3').children('.peso-total').html("");
      }
    }).
    always(function(){
      jQuery('#overlay').hide();
    });
    // Fim percorre todas as postagens
  });
  // Fim percorre todas as colunas
}

function loadComments(post_id) {
  jQuery("ul.comment-list").html("");
  jQuery.ajax({
    url: wpApiSettings.root + 'wp/v2/comments?post=' + post_id,
    type: 'get',
    success: function(response) {
      if (response.length > 0) {
        jQuery(".comment-message").html("");
        jQuery.each(response, function(idx, value) {
          if (value.author_name == "") {
            author_name = "Sem nome de usuario";
          } else {
            author_name = value.author_name;
          }

          inicio = "<li id=" + value.id + ">";
          usuario = '<span class="user-guid"> <img src="' + jQuery("#img_user").attr('src') + '" width="27"/> ' + author_name + '</span>';
          if (userid == value.author) {
            conteudo = '<a href="#" class="edit-comment">Editar</a>';
          } else {
            conteudo = "";
          }
          conteudo += value.content.rendered;
          fim = "</li>";
          concat = inicio.concat(usuario, conteudo, fim);
          jQuery("ul.comment-list").append(concat);
        });
      } else {
        message = "Sem comentários";
        content.children(".comment-message").html(message);
      }
    },
    complete: function(){
      jQuery('.load-pik img').hide();
    }
  });
}

function updateComment(id, content) {
  jQuery.ajax({
    url: wpApiSettings.root + 'wp/v2/comments/' + id,
    type: 'post',
    data: {
      "content": content
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
      jQuery('.load-pik img').css({
        'display': 'table'
      });
    },
    success: function(response) {
      loadComments(response.post);
    }
  });
}

function addTag(id, post_id, post_type){
  if (jQuery.inArray(parseInt(id), tags) >= 0) {
    console.error('Já existe');
  } else {
    tags.push(parseInt(id));
    jQuery.ajax({
      url:  wpApiSettings.root + 'wp/v2/' + post_type + '-api/' + post_id,
      type: 'post',
      data: {
        "tags": tags
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
      },
      success: function(data){
        if(data.tags.length > 0){
          content.children('.tags-list').html('');
            jQuery.each(data.tags, function(idx, val){
              jQuery.ajax({
                url: wpApiSettings.root + 'wp/v2/tags/' + val,
                type: 'get',
                success: function(response) {
                  inicio = '<a href="#" data-termid="' + response.id + '" style="background-color:' + response.description + ';border-color: ' + response.description + '" class="tag-ativa button">' ;
                  tag = response.slug;
                  fim = '</a>';
                  concat = inicio.concat(tag, fim);
                  content.children('.tags-list').append(concat);
                }
              });
            });
        }
      }
    });
  }
}

function removeTag(id, post_id, post_type){
  // Pega a posição atual do membro na lista global
  var pos = tags.indexOf(id);

  // Recorta do Array o term_id do evento
  tags.splice(pos, 1);

  // Verifica e trata o Array se o mesmo estiver vazio
  if (jQuery.isArray(tags)) {
    if (tags.length == 0) {
      tags[0] = "";
    }
  }

  jQuery.ajax({
    url:  wpApiSettings.root + 'wp/v2/' + post_type + '-api/' + post_id,
    type: 'post',
    data: {
      "tags": tags
    },
    beforeSend: function(xhr) {
      xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
    },
    success: function(response){
      content.children('.tags-list').html('');
      if(response.tags.length > 0){
          jQuery.each(response.tags, function(idx, val){
            jQuery.ajax({
              url: wpApiSettings.root + 'wp/v2/tags/' + val,
              type: 'get',
              success: function(data) {
                inicio = '<a href="#" data-termid="' + response.id + '" style="background-color:' + data.description + ';border-color: ' + data.description + '" class="tag-ativa button">' ;
                tag = data.slug;
                fim = '</a>';
                concat = inicio.concat(tag, fim);
                content.children('.tags-list').append(concat);
              }
            });
          });
      }
    }
  });
}
////////////////////
//-->> ENTRY POINT
////////////////////

jQuery(document).ready(function($) {
  pesoTarefas($);

  //organiza o box das colunas
  organizaColunas($);

  //configura os drags
  adicionaListenersDragNDrop($);
  adicionaDrag($);

  // Ações da pagina de Cards

  jQuery('.add-card-actived').click(function() {
    var show = jQuery(this).parent('.add-card');

    if (show.is(':visible')) {
      return;
    } else {
      jQuery(this).parent().children('.add-card').show();
      jQuery(this).hide();
    }
  });

  jQuery(".add-card").on('click', '.close-add-card', function() {
    jQuery(this).parent().parent().hide();
    jQuery(this).parent().parent().parent().children('.add-card-actived').show();
  });

  jQuery(".modal .close").click(function() {
    jQuery(this).parent('.row').children('.cancel-editable, .save-editable').hide();
    jQuery(this).parent('.row').children('.m-title').prop('contenteditable', false);
    jQuery(this).parent().parent().children('.row').children('.m-content').prop('contenteditable', false);
    jQuery(this).parent().parent().children('.row').children('.cancel-editable, .save-editable').hide();
    jQuery("ul.comment-list").empty();
    jQuery(this).parent().parent().parent('.modal').hide();
  });

  jQuery(window).click(function(event) {
    if (jQuery(event.target).hasClass('modal')) {
      jQuery("ul.comment-list").empty();
      jQuery(event.target).children('.modal-content').children('.row').children('.cancel-editable, .save-editable').hide();
      jQuery(event.target).children('.modal-content').children('.row').children('.m-title, .m-content').prop('contenteditable', false);
      jQuery(event.target).hide();
    }
  });

  // Carrega as informações para o modal
  jQuery(".tarefa ul li a.open-modal").click(function() {
    var modal = jQuery('.modal');
    var content = modal.children('.modal-content').children('.row');
    var postId = jQuery(this).attr('data-postid');
    var postType = jQuery(this).attr('data-posttype');

    jQuery.ajax({
      url: wpApiSettings.root + 'wp/v2/' + postType + '-api/' + postId,
      type: 'get',
      beforeSend: function() {
        jQuery('#overlay').show();
      },
      success: function(data) {
        var peso = data.esforco;
        tags = data.tags;
        var esf_realizado = data.esforco_realizado;

        content.children('.membros-atual, .membros-list, .tags-list').html("");
        content.children('h4').attr({
          'data-postid': data.id,
          'data-posttype': data.type
        }).html(data.title.rendered); // Atribui o titulo
        content.children('form').attr({
          'data-postid': data.id
        });

        // Verifica se tem conteudo escrito,
        // se não adiciona um texto 'Sem descrição' no campo
        if (data.content.rendered == '') {
          content.children('.m-content').html('<b class="sem-content">Sem descrição.</b>');
          content.children('.m-content').attr({
            'data-postid': data.id,
            'data-posttype': data.type
          });
        } else {
          content.children('.m-content').html(data.content.rendered);
          content.children('.m-content').attr({
            'data-postid': data.id,
            'data-posttype': data.type
          });
        }

        if (peso == 0) {
          jQuery('select.peso-tarefa[id="esforco"]').val("1");
        } else {
          peso = peso.toString();
          jQuery('select.peso-tarefa[id="esforco"]').val(peso);
        }

        if (esf_realizado == 0) {
          jQuery('select.peso-tarefa[id="esforco_realizado"]').val("1");
        } else {
          esf_realizado = esf_realizado.toString();
          jQuery('select.peso-tarefa[id="esforco_realizado"]').val(esf_realizado);
        }

        // Carrega membros
        team = data.team_api;

        if (data.team_api.length > 0) {
          jQuery.each(data.team_api, function(idx, val) {
            jQuery.ajax({
              url: wpApiSettings.root + 'wp/v2/team_api/' + val,
              type: 'get',
              success: function(response) {
                inicio = '<a href="#" data-termid="' + response.id + '" class="membro-ativo button">';
                membro = response.slug;
                fim = '</a>';
                concat = inicio.concat(membro, fim);
                content.children('.membros-atual').append(concat);
              }
            });
          });
        }

        jQuery.ajax({
          url: wpApiSettings.root + 'wp/v2/team_api/',
          type: 'get',
          success: function(response) {
            jQuery.each(response, function(idx, val) {
              inicio = '<option ';
              membro = 'value="' + val.id + '">' + val.name;
              fim = '</option>';
              concat = inicio.concat(membro, fim);
              content.children('.membros-list').append(concat);
            });
          }
        });
        // Fim carrega membros

        if(data.tags.length > 0){
            jQuery.each(data.tags, function(idx, val){
              jQuery.ajax({
                url: wpApiSettings.root + 'wp/v2/tags/' + val,
                type: 'get',
                success: function(response) {
                  inicio = '<a href="#" data-termid="' + response.id + '" style="background-color:' + response.description + ';border-color: ' + response.description + '" class="tag-ativa button">' ;
                  tag = response.slug;
                  fim = '</a>';
                  concat = inicio.concat(tag, fim);
                  content.children('.tags-list').append(concat);
                }
              });
            });
        }

        // Carrega tags
        jQuery.ajax({
          url: wpApiSettings.root + 'wp/v2/tags',
          type: 'get',
          success: function(response) {
            content.children('.tags_color').html('');
            jQuery.each(response, function(idx, val) {
              inicio = '<option ';
              tag = 'value="' + val.id + '">' + val.name;
              fim = '</option>';
              concat = inicio.concat(tag, fim);
              content.children('.tags_color').append(concat);
            });
          }
        })
        // Fim carrega tags

        // Carrega os comentários
        jQuery.ajax({
          url: wpApiSettings.root + 'wp/v2/comments?post=' + postId,
          type: 'get',
          beforeSend: function(xhr) {
            xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
            jQuery('.load-pik img').css({
              'display': 'table'
            });
          },
          success: function(response) {
            if (response.length > 0) {
              content.children(".comment-message").html("");
              jQuery.each(response, function(idx, value) {
                if (value.author_name == "") {
                  author_name = "Sem nome de usuario";
                } else {
                  author_name = value.author_name;
                }

                inicio = "<li id=" + value.id + ">";
                usuario = '<span class="user-guid"> <img src="' + jQuery("#img_user").attr('src') + '" width="27"/> ' + author_name + '</span>';
                if (userid == value.author) {
                  conteudo = '<a href="#" class="edit-comment">Editar</a>';
                } else {
                  conteudo = "";
                }
                conteudo += value.content.rendered;
                fim = "</li>";
                concat = inicio.concat(usuario, conteudo, fim);
                jQuery("ul.comment-list").append(concat);
              });
            } else {
              message = "Sem comentários";
              content.children(".comment-message").html(message);
            }
          },
          complete: function() {
            jQuery('.load-pik img').hide();
          }
        });
        // Fim carrega comentários
      },
      complete: function() {
        jQuery('#overlay').hide();
      },
    });

    if (modal.is(':visible')) {
      modal.hide();
    } else {
      modal.show();
    }
  });

  // Envia a
  jQuery('.comment-form form').submit(function() {
    var form = jQuery(this);
    var author_name = jQuery("#author_name").val();
    var author = jQuery("#user_id").val();
    var content = jQuery(this).serializeArray()[0].value;
    var postId = jQuery(this).data('postid');

    // console.log(content);
    jQuery.ajax({
      url: wpApiSettings.root + 'wp/v2/comments',
      type: 'post',
      data: {
        "author": author,
        "author_name": author_name,
        "post": postId,
        "content": content
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
      },
      success: function(response) {
        form.children('[name="comment-content"]').val("");
        // Atualiza a lista de comentarios
        jQuery.ajax({
          url: wpApiSettings.root + 'wp/v2/comments?post=' + postId,
          type: 'get',
          beforeSend: function(xhr) {
            xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
            jQuery("ul.comment-list").empty();
            jQuery("span.comment-message").html("");
            jQuery('.load-pik img').css({
              'display': 'table'
            });
          },
          success: function(data) {
            if (data.length > 0) {
              jQuery.each(data, function(idx, value) {
                if (value.author_name == "") {
                  author_name = "Sem nome de usuario";
                } else {
                  author_name = value.author_name;
                }

                inicio = "<li id=" + value.id + ">";
                usuario = '<span class="user-guid"> <img src="' + jQuery("#img_user").attr('src') + '" width="27"/> ' + author_name + '</span>';
                if (userid == value.author) {
                  conteudo = '<a href="#" class="edit-comment">Editar</a>';
                } else {
                  conteudo = "";
                }
                conteudo += value.content.rendered;
                fim = "</li>";
                // console.log(jQuery("#img_user"));
                concat = inicio.concat(usuario, conteudo, fim);
                jQuery("ul.comment-list").append(concat);
              });
            } else {
              message = "Sem comentários";
              content.children(".comment-message").html(message);
            }
          },
          complete: function() {
            jQuery('.load-pik img').hide();
          }
        });
        // Fim do atualiza lista comentarios
      }
    });
    return false;
  });

  jQuery('.content-editable, .title-editable').on('click', 'a.cancel-editable', function() {
    if (jQuery(this).is(':visible')) {
      jQuery(this).parent().children('.m-title, .m-content').blur().prop('contenteditable', false);
      if (jQuery(this).parent().children('.m-content').is(':empty')) {
        jQuery(this).parent().children('.m-content').html('<b class="sem-content">Sem descrição.</b>');
      }
      jQuery(this).parent().children('.save-editable').hide();
      jQuery(this).hide();
    }
  });

  jQuery('.content-editable, .title-editable').on('click', 'a.save-editable', function() {
    if (jQuery(this).is(':visible')) {
      var Edit = jQuery(this).parent().children('.m-title, .m-content');
      var postType = jQuery(this).parent().children('.m-title, .m-content').attr('data-posttype');
      var postId = jQuery(this).parent().children('.m-title, .m-content').attr('data-postid');
      var btn = jQuery(this);

      if (Edit.hasClass('m-title')) {
        var dataPOST = {
          "title": Edit.html()
        };
      } else if (Edit.hasClass('m-content')) {
        var dataPOST = {
          "content": Edit.html()
        };
      }

      $.ajax({
        url: wpApiSettings.root + 'wp/v2/' + postType + '-api/' + postId,
        method: 'POST',
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        },
        data: dataPOST
      }).done(function(response) {
        // console.log( response );
        jQuery('a.open-modal[data-postid=' + postId + ']').html(response.title.rendered);
        Edit.prop('contenteditable', false);
        btn.parent().children('.cancel-editable').hide();
        btn.hide();
      });
    }
  });

  jQuery('.content-editable, .title-editable').on('click', '.edit-content', function() {
    var parent = jQuery(this).parent();
    parent.children('.m-content').children('.sem-content').remove();
    parent.children('.cancel-editable, .save-editable').css({
      'display': 'inline-block'
    });
    parent.children('.m-content, .m-title').prop('contenteditable', true).focus();
  });

  // Atualiza os esforços
  jQuery('.peso-tarefa').change(function() {
    var peso = parseInt(jQuery(this).val());
    var postType = jQuery(this).parent().children('h4').data('posttype');
    var postId = jQuery(this).parent().children('h4').data('postid');
    var send = {};

    send[jQuery(this).prop('id')] = peso;

    jQuery.ajax({
      url: wpApiSettings.root + 'wp/v2/' + postType + '-api/' + postId,
      method: 'POST',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
      },
      data: send,
      success: function(response) {
        console.log(response);
        pesoTarefas($);
      },
      error: function(response) {
        console.error(response);
      }
    });
  });
  // FIM Atualiza os esforços

  // Evento de adicionar membros a um card
  jQuery('.add-membro-active').click(function() {
    var membrosAtual = jQuery(this).parent().children('.membros-atual');
    var slugAdd = parseInt(jQuery(this).parent().children('.membros-list').val());
    var postType = jQuery(this).parents('.modal-content').children('.title-editable').children('.m-title').data('posttype');
    var postId = jQuery(this).parents('.modal-content').children('.title-editable').children('.m-title').data('postid');

    if (jQuery.inArray(slugAdd, team) >= 0) {
      console.error('Já existe');
    } else {
      membrosAtual.html("");
      team.push(slugAdd);

      // Inicio da Atualização
      jQuery.ajax({
        url: wpApiSettings.root + 'wp/v2/' + postType + '-api/' + postId,
        type: 'post',
        data: {
          "team_api": team
        },
        beforeSend: function(xhr) {
          xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
        },
        success: function(response) {
          // Atualiza a variavel global
          team = response.team_api;

          // Atualiza os botões de usuarios atuais
          if (response.team_api.length > 0) {
            jQuery.each(response.team_api, function(idx, val) {
              jQuery.ajax({
                url: wpApiSettings.root + 'wp/v2/team_api/' + val,
                type: 'get',
                success: function(data) {
                  inicio = '<a href="#" data-termid="' + data.id + '" class="membro-ativo button">';
                  membro = data.slug;
                  fim = '</a>';
                  concat = inicio.concat(membro, fim);
                  membrosAtual.append(concat);
                }
              });
            });
          }
          // fim atualiza botões
        }
      });
      // Fim da atualização
      pesoTarefas($);
    }
  });

  // Evento de remover membros de um card
  jQuery(document).on('click', '.membro-ativo', function() {
    // Carrega os locais para tratamentos do DOM
    var membrosAtual = jQuery(this).parents('.row').children('.membros-atual');
    var postType = jQuery(this).parents('.modal-content').children('.title-editable').children('.m-title').data('posttype');
    var postId = jQuery(this).parents('.modal-content').children('.title-editable').children('.m-title').data('postid');
    var term_id = jQuery(this).data('termid');

    // Pega a posição atual do membro na lista global
    var pos = team.indexOf(term_id);
    var team_cp = team;

    // Recorta do Array o term_id do evento
    team_cp.splice(pos, 1);

    // Verifica e trata o Array se o mesmo estiver vazio
    if (jQuery.isArray(team_cp)) {
      if (team_cp.length == 0) {
        team_cp[0] = "";
      }
    }

    // Inicio da Atualização
    jQuery.ajax({
      url: wpApiSettings.root + 'wp/v2/' + postType + '-api/' + postId,
      type: 'post',
      data: {
        "team_api": team_cp
      },
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-WP-Nonce', wpApiSettings.nonce);
      },
      success: function(response) {
        // Atualiza a variavel global
        team = response.team_api;

        // Limpa a lista atual de membros ativos
        membrosAtual.html("");

        if (response.team_api.length > 0) {
          // Gera a lista de usuario ativos
          jQuery.each(response.team_api, function(idx, val) {
            jQuery.ajax({
              url: wpApiSettings.root + 'wp/v2/team_api/' + val,
              type: 'get',
              success: function(data) {
                inicio = '<a href="#" data-termid="' + data.id + '" class="membro-ativo button">';
                membro = data.slug;
                fim = '</a>';
                concat = inicio.concat(membro, fim);
                membrosAtual.append(concat);
              }
            });
          });
          // fim gera lista de usuario ativos
        }
        pesoTarefas($);
      }
    });
    // Fim da atualização
  });
  // Fim Evento de remover membros de um card

  // Evento de editar um comentario
  jQuery('.comment-list').on('click', '.edit-comment', function() {

    var botaoSalvar = '<a href="#" class="save-editable button">Salvar</a>';
    var botaoCancelar = '<a href="#" class="cancel-editable button button-warning">Cancelar</a>';

    // Torna o paragrafo vizinho editavel
    jQuery(this).parent().children('p').prop('contenteditable', true).focus();

    // Desativa um duplo clique no 'Editar'
    jQuery(this).prop('disabled', true);

    // Adiciona os botões de ações
    jQuery(this).parent().append(botaoSalvar);
    jQuery(this).parent().append(botaoCancelar);

    // Deixa os botões visiveis
    jQuery(this).parent().children('.save-editable, .cancel-editable').css({
      "display": "inline-block",
      "width": "inherit",
      "margin": "1rem"
    });
  });
  // Fim evento de editar comentario

  // Evento de salvar comentario editado
  jQuery('.comment-list').on('click', 'a.save-editable', function() {
    var content = jQuery(this).parent().children('p').html();
    var id = jQuery(this).parent().attr('id');
    updateComment(id, content);
    jQuery(this).parent().children('p').blur().prop('contenteditable', false);
    jQuery(this).parent().children('.cancel-editable').remove();
    jQuery(this).parent().children('.edit-comment').prop('disabled', false);
    jQuery(this).remove();
  });
  // fim Evento de salvar comentario editado

  // Evento que cancela edição em comentario
  jQuery('.comment-list').on('click', 'a.cancel-editable', function() {
    if (jQuery(this).is(':visible')) {
      jQuery(this).parent().children('p').blur().prop('contenteditable', false);
      jQuery(this).parent().children('.save-editable').remove();
      jQuery(this).parent().children('.edit-comment').prop('disabled', false);
      jQuery(this).remove();
    }
  });
  // Fim evento que cancela edição em comentario

  // Evento de adicionar Etiqueta
  jQuery('a.add-tag').on('click', function(){
    var tagID = jQuery(this).parent().children('.tags_color').val();
    var postType = jQuery(this).parents('.modal-content').children('.title-editable').children('.m-title').data('posttype');
    var post_id = jQuery(this).parents('.modal-content').children('.title-editable').children('.m-title').data('postid');

    addTag(tagID, post_id, postType);
    pesoTarefas($);
  });
  // Fim evento de adicionar Etiqueta

  // Evento para remover tags
  jQuery(document).on('click', '.tag-ativa', function() {
    var postType = jQuery(this).parents('.modal-content').children('.title-editable').children('.m-title').data('posttype');
    var postId = jQuery(this).parents('.modal-content').children('.title-editable').children('.m-title').data('postid');
    var term_id = jQuery(this).data('termid');

    removeTag(term_id, postId, postType);
    pesoTarefas($);
  });
  // fim Evento para remover tags

  // tamanho minimo da classe colunas-items
  var footerSize = $('footer').height();
  var domHeight = $('div.container').height();
  var colunasHeight = domHeight - footerSize;

  $('.colunas-items').css({
    'min-height': colunasHeight + 'px'
  });
  // Fim ações da página de Cards

  // Ações da página de editar projeto

  var visibledTerm = jQuery(".term-fields");

  jQuery('#add-term-button').click(function() {
    if (visibledTerm.is(':visible')) {
      visibledTerm.children('[type^="text"]:first').bind().clone().val("").appendTo(visibledTerm);
    } else {
      visibledTerm.show();
      jQuery("#exclude-term-button").show();
    }
  });

  jQuery('#exclude-term-button').click(function() {
    var sizeElem = visibledTerm.children('[type^="text"]').length;
    if (sizeElem > 1) {
      visibledTerm.children('[type^="text"]:last').remove();
    } else {
      jQuery(this).hide();
      visibledTerm.hide().children('[type^="text"]').val('');
    }
  });
});
