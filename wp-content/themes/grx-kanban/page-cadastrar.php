<?php get_header(); ?>
<main>
  <?php if (have_posts()) : ?>

  	<?php while (have_posts()) : the_post(); ?>
  		  <div class="row">
            <h3><?php the_title(); ?></h3>
  		  </div>
        <div class="row">
            <form method="post" action="<?php echo get_template_directory_uri(); ?>/actions/cadastrar-usuario.php" id="cadastrar-usuario">
              <div class="twelve columns">
                <label for="nome">Nome</label>
                <input type="text" class="u-full-width" name="nome" id="nome" placeholder="Digite seu nome" required/>
              </div>
              <div class="row">
                <div class="six columns">
                  <label for="nickname">Nickname</label>
                  <input type="text" class="u-full-width" name="nickname" id="nickname" placeholder="Digite um nickname" maxlength="20" required/>
                </div>
                <div class="six columns">
                  <p>Maximo de 20 caracteres.</p>
                </div>
              </div>
              <div class="row">
                <div class="six columns">
                  <label for="email">E-mail</label>
                  <input type="email" name="email" id="email" class="u-full-width" placeholder="Digite seu e-mail" autocomplete="off" required/>
                </div>
                <div class="six columns">
                  <label for="senha">Senha</label>
                  <input type="password" name="senha" id="senha" class="u-full-width" placeholder="Senha" autocomplete="off" required/>
                </div>
              </div>
              <label for="funcao">Função (Cargo)</label>
              <select class="u-full-width" name="funcao" id="funcao" required>
                <option value="">Selecione uma função</option>
                <option value="colaborador">Colaborador</option>
                <option value="estagio">Estagiário</option>
              </select>
              <input type="hidden" id="url" value="<?php echo get_template_directory_uri();?>/actions/cadastrar-usuario.php">
              <input type="submit" class="button-primary" value="<?php _e('Register'); ?>">
            </form>
        </div>
  	<?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>
