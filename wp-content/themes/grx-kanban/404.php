<?php get_header(); ?>
<main>
  <div class="row centered">
    <div class="">
      <h1><strong>ERROR 404</strong> <img src="<?php echo get_template_directory_uri().'/inc/img/error-404.svg'?>" alt="Error 404" /></h1>
    </div>
  </div>
</main>
<?php
get_footer();
