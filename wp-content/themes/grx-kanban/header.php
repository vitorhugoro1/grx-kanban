<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo bloginfo('name'); ?></title>
    <?php wp_head(); ?>
  </head>
  <body class="u-full-width">
    <div class="container"> <!-- Abrindo a div container -->
      <div class="row top">
        <h3 class="page-title u-pull-left one-half column"><a href="<?php echo home_url(); ?>">GRX - Kanban for Scrum</a></h3>
        <?php if(is_user_logged_in()) { ?>
            <ul class="u-pull-right user-items one-half column">
              <li>
                <a href="#">
                  <input type="hidden" id="userid" value="<?php echo get_current_user_id(); ?>">
                  <input type="hidden" id="author_name" value="<?php echo get_the_author_meta( 'display_name', get_current_user_id() );?>">
                  <img src="<?php echo get_template_directory_uri(); ?>/inc/img/user-profile.svg" id="img_user" /><?php echo get_the_author_meta( 'display_name', get_current_user_id() ); ?>
                </a>
                <ul>
                  <li> <a href="<?php echo get_permalink_by('title', 'Perfil'); ?>">Perfil</a> </li>
                  <li> <a href="<?php echo wp_logout_url(home_url()); ?>">Sair</a> </li>
                </ul>
              </li>
            </ul>
        <?php } ?>
      </div>
