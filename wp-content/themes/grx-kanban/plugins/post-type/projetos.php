<?php

 ?>
<div class="wrap">
    <h2><?php echo get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <a href="<?php echo admin_url('admin.php?page=projetos'); ?>" class="nav-tab nav-tab-active"><?php echo __('General'); ?></a>
        <a href="<?php echo admin_url('admin.php?page=projeto-alterar'); ?>" class="nav-tab">Alterar status</a>
    </h2>
    <form method="post">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>">
        <input type="hidden" name="action" value="save">
        <div class="inside">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                        <label for="nome-projeto">Nome do projeto</label>
                    </th>
                    <td>
                        <input type="text" name="nome-projeto" id="nome-projeto" class="regular-text" placeholder="Nome do projeto" value="" maxlength="20" required>
                        <p class="description">Máximo de 20 caracteres</p>
                    </td>
                </tr>
            </table>
        </div>
        <p class="submit"><?php submit_button(__('Save'), 'button-primary'); ?></p>
    </form>
</div>
