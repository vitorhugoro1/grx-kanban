<?php
error_reporting(1);
add_action('admin_menu', 'menus');

function menus(){
    add_menu_page( 'Adicionar projeto' ,'Projetos', 'manage_options', 'projetos', 'projetos');
    add_submenu_page('projetos', 'Alterar status', 'Alterar status', 'manage_options', 'projeto-alterar', 'actions_projetos');
}
/**
 * [projetos description]
 * @return [type] [description]
 */

function projetos(){
    if(isset($_POST['action']) && $_POST['action'] == 'save'){
    if(get_option('opt-post_type') !== '' || get_option('opt-post_type') !== FALSE) {
        $options = unserialize(get_option('opt-post_type'));
        if(get_option('opt-status_projeto') !== '' || get_option('opt-status_projeto') !== FALSE){
            $status = unserialize(get_option('opt-status_projeto'));
        } else {
            $status = array();
        }
    } else {
        $options = array();
        if(get_option('opt-status_projeto') !== '' || get_option('opt-status_projeto') !== FALSE){
            $status = unserialize(get_option('opt-status_projeto'));
        } else {
            $status = array();
        }
    }

    if(!in_array($_POST['nome-projeto'],$options)){
        $slug = sanitize_title($_POST['nome-projeto']);
        $status[$slug] = '1';
        array_push($options, $_POST['nome-projeto']);
        update_option('opt-post_type', serialize($options));
        update_option('opt-status_projeto', serialize($status));
    } else {
        $message = 'error';
    }
}

    /*
     * Carrega o template do formulario
     */
    require 'projetos.php';
}
/**
 * [actions_projetos description]
 * @return [type] [description]
 */

function actions_projetos(){
    if(isset($_POST['action']) && $_POST['action'] == 'update'){
        $post_types = unserialize(get_option('opt-post_type'));
        $status = unserialize(get_option('opt-status_projeto'));
        foreach($post_types as $post_type){
            $post_type = sanitize_title($post_type);
            $status[$post_type] = $_POST['status-'.$post_type];
        }
        update_option('opt-status_projeto', serialize($status));
    }
    require 'alterar.php';
}

/**
 * Verifica se tem projetos salvos
 * e gera os respectivos post type e taxonomies de tarefas
 */
add_action('init', 'init_projetos');

function init_projetos(){
    add_option('opt-post_type', serialize(array()));
    add_option('opt-status_projeto',serialize(array()));
    $post_types = unserialize(get_option('opt-post_type')); // Carrega um array com os projetos já salvos
    $status = unserialize(get_option('opt-status_projeto'));

    if(!empty($post_types)){
        foreach($post_types as $name){
            $singular_label = $name;
            $post_type = sanitize_title($name); // Formata o nome do projeto para gerar um slug
            $labels = array(
                'name'					=> __($singular_label),
                'singular_name'			=> __($singular_label),
                'add_new'				=> __('Adicionar novo'),
                'add_new_item'			=> __('Adicionar novo').' '.$singular_label,
                'edit_item'				=> __('Editar').' '.$singular_label,
                'new_item'				=> __('Novo').' '.$singular_label,
                'view_item'				=> __('Ver').' '.$singular_label,
                'search_items'			=> __('Procurar').' '.$singular_label,
                'not_found'				=> __('Nada encontrado'),
                'not_found_in_trash'	=> __('Nada encontrado no lixo')
            );

            $args = array(
                'labels'                => $labels,
                'public'				=> true,
                'publicly_queryable'	=> true,
                'show_ui'				=> true,
                'query_var'				=> true,
                'capability_type'		=> 'post',
                'hierarchical'			=> true,
                'menu_position'			=> 7,
                'menu_icon'				=> 'dashicons-portfolio',
                'has_archive'			=> true,
                'taxonomies' => array('post_tag'),
                'exclude_from_search'	=> true,
                'show_in_rest'       => true,
                'rest_base'          => $post_type.'-api',
                'rest_controller_class' => 'WP_REST_Posts_Controller',
                'supports'				=> array('title', 'editor', 'comments')
            );

            register_post_type($post_type, $args);

            $singular_label = 'tarefa';
            $taxonomy = 'tarefas_'.$post_type;

            $labels = array(
                'name'              => __( 'Tarefas'),
                'singular_name'     => __( $singular_label),
                'search_items'      => __( 'Procurar '.$singular_label ),
                'all_items'         => __( 'All '.$singular_label ),
                'parent_item'       => __( 'Parent '.$singular_label ),
                'parent_item_colon' => __( 'Parent '.$singular_label.':' ),
                'edit_item'         => __( 'Editar '.$singular_label ),
                'update_item'       => __( 'Atualizar '.$singular_label ),
                'add_new_item'      => __( 'Adicionar nova '.$singular_label ),
                'new_item_name'     => __( 'Nova '.$singular_label ),
                'menu_name'         => __('Tarefas'),
            );

            $args = array(
                'hierarchical'      => true,
                'labels'            => $labels,
                'show_ui'           => true,
                'show_admin_column' => true,
                'query_var'         => true,
                'show_in_rest'      => true,
                'rest_base'         => 'tax-'.$taxonomy,
                'rest_controller_class' => 'WP_REST_Terms_Controller'
              );

            register_taxonomy($taxonomy, $post_type, $args);
            $testes = get_terms($taxonomy, array('hide_empty' => false));
            if(count($testes) == 0){
              $colunas = array(
                1 => 'Pré Game',
                2 => 'Sprint',
                3 => 'Fazendo',
                4 => 'Concluído',
                5 => 'Impedimento',
                6 => 'Arquivo'
              );

              foreach($colunas as $key => $term){
                $validation = wp_insert_term( $term, $taxonomy, array('slug' => sanitize_title($term)) );

                if(is_wp_error( $validation )){
                  die(is_wp_error( $validation ));
                } else {
                  update_term_meta($validation['term_id'], 'ordem', intval($key));
                }
              }
            }
            flush_rewrite_rules();
        }
    }
}

/**
 * [remove_menus description]
 * @return [type] [description]
 */

function remove_menus(){
    $post_types = unserialize(get_option('opt-post_type')); // Carrega um array com os projetos já salvos
    $status = unserialize(get_option('opt-status_projeto'));

    if(!empty($post_types)) {
        foreach ($post_types as $name) {
            $post_type = sanitize_title($name);
            $taxonomy = 'tarefas_'.$post_type.'div';
            /**
             * Verifica se o projeto está desativado
             * e remove do menu
             */
            if ($status[$post_type] == '0') {
                remove_menu_page('edit.php?post_type=' . $post_type);
            }
            // Remove o box de taxonomies original
            remove_meta_box($taxonomy, $post_type, 'side');
        }
    }
}

add_action('admin_menu', 'remove_menus');

/**
 * [projetos_front description]
 * @return [type] [description]
 */

function projetos_front(){
  $post_types = unserialize(get_option( 'opt-post_type' ));
  $status = unserialize(get_option('opt-status_projeto'));

  if(!empty($post_types)) {
      foreach ($post_types as $name) {
        $post_type = sanitize_title($name);
        if($status[$post_type] == '1'){
            $arr['ativo'][] = $post_type;
        } else if($status[$post_type] == '0'){
            $arr['inativo'][] = $post_type;
        }
      }
  }

  return $arr;
}
