<div class="wrap">
    <h2><?php echo get_admin_page_title(); ?></h2>
    <h2 class="nav-tab-wrapper">
        <a href="<?php echo admin_url('admin.php?page=projetos'); ?>" class="nav-tab "><?php echo __('General'); ?></a>
        <a href="<?php echo admin_url('admin.php?page=projeto-alterar'); ?>" class="nav-tab nav-tab-active">Alterar status</a>
    </h2>
    <form method="post">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page']; ?>">
        <input type="hidden" name="action" value="update">
        <table class="form-table">
            <?php
                $post_types = unserialize(get_option('opt-post_type'));
                $status = unserialize(get_option('opt-status_projeto'));
                foreach ($post_types as $post_type){
                    $current = $status[sanitize_title($post_type)];
                    ?>
                    <tr valign="top">
                        <th scope="row">
                            <label for="<?php echo sanitize_title($post_type); ?>"><?php echo $post_type; ?></label>
                        </th>
                        <td>
                            <select name="status-<?php echo sanitize_title($post_type); ?>" id="<?php echo sanitize_title($post_type); ?>">
                                <option value="1" <?php echo ($current == '1') ? 'selected' : ''; ?>>Ativo</option>
                                <option value="0" <?php echo ($current == '0') ? 'selected' : ''; ?>>Desativado</option>
                            </select>
                        </td>
                    </tr>
            <?php
                }
            ?>
        </table>
        <p class="submit"><?php submit_button(__('Update'), 'button-primary'); ?></p>
    </form>
</div>