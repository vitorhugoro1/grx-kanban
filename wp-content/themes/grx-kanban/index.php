<?php get_header(); ?>
<main>
  <?php if (!is_user_logged_in()) {
    ?>
    <div class="row centered">
        <div class="one-half column">
          <a class="button u-pull-right" href="<?php echo get_permalink_by('title', 'Cadastrar');
    ?>">Cadastrar</a>
        </div>
        <div class="one-half column">
          <a class="button u-pull-left" href="<?php echo get_permalink_by('title', 'Login');
    ?>">Logar</a>
        </div>
    </div>
   <?php
} else {
  $projetos_front = projetos_front();
  if(!empty($projetos_front)){
    foreach($projetos_front as $key => $value){
      $projetos = get_the_author_meta('projetos', wp_get_current_user()->ID);

      if(empty($projetos) && !(wp_get_current_user()->roles[0] == 'administrator')){
        ?>
        <div class="row n-projeto">
          <h3><small> Não há projetos </small></h3>
          <p>
            Crie um projeto, ou peça ao administrador de um projeto ativo para adiciona-lo.
          </p>
        </div>
        <?php
        break;
      }
      if('ativo' == $key){
        ?>
      <div class="row projetos">

          <h3><small>Projetos ativos</small></h3>
        <?php
        foreach($value as $val){
          ?>
          <div class="three columns">
            <h4><a href="<?php echo get_post_type_archive_link( sanitize_title($val) ); ?>"><?php echo $val; ?></a></h4>
          </div>
          <?php
        }
        ?>
      </div>
        <?php
      } else if('inativo' == $key){
        ?>
          <div class="row projetos">
              <h3><small>Projetos inativos</small></h3>
            <?php
            foreach($value as $val){
              ?>
              <div class="three columns">
                <h4><a href="<?php echo get_post_type_archive_link( sanitize_title($val) ); ?>"><?php echo $val; ?></a></h4>
              </div>
              <?php
            }
            ?>
          </div>
        <?php
      }
    }
  } else {
    ?>
      <b>Sem projetos</b>
    <?php
  }
    ?>
    <div class="u-full-width u-cf">
      <a class="button u-pull-right" href="<?php echo get_permalink_by('title', 'Novo projeto'); ?>">Criar novo projeto</a>
    </div>
  <?php
} ?>
</main>
<?php
get_footer();
