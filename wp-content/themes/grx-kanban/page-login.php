<?php get_header(); ?>

<?php

  $args = array(
    'echo'      => true,
    'redirect'  => home_url(),
    'form_id'   => 'loginform',
    'label_username'  => 'E-mail',
    'label_password'  => 'Senha',
    'label_log_in'    => 'Acessar',
    'remember'       => true,
    'value_username' => NULL,
    'value_remember' => false
  );

 ?>
<main>
  <?php if (have_posts()) : ?>

  	<?php while (have_posts()) : the_post(); ?>
  		  <div class="row">
            <h3><?php the_title(); ?></h3>
  		  </div>
        <div class="row centered">
          <?php
            if ( $_REQUEST['login'] == 'failed' )
                echo '<p style="color:red;">Usu&aacute;rio ou senha incorretos.</p>';
            ?>
            <?php wp_login_form($args); ?>

        </div>
  	<?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>
