<?php
add_action('cmb2_init', 'meta_boxes');
/**
 * Cria os meta boxes automaticamente
 */
function meta_boxes(){
    global $post_type;
    $taxonomy = (isset($post_type)) ? 'tarefas_'.$post_type : 'tarefas_'.$_REQUEST['post_type'];
    $post_types = post_type_list();

    $cmb = new_cmb2_box(array(
        'id' => 'meta_boxes',
        'title' => 'Informações',
        'object_types'  => $post_types,
        'context'   => 'normal',
        'priority'  => 'high',
        'show_names'    => true
    ));

    $cmb->add_field(array(
        'name'  => 'Tarefa',
        'taxonomy'  => $taxonomy,
        'id'        => 'task',
        'type'      => 'taxonomy_radio',
        'show_option_none' => false,
        'attributes'    => array(
            'required'  => 'required'
        )
    ));

    $cmb->add_field(array(
        'name'  => 'Data de Entrega',
        'type'  => 'text_date_timestamp',
        'id'    => 'data_entrega',
        'attributes'   => array(
            'placeholder'   => 'dd/mm/aaaa'
        )
    ));

    $cmb->add_field(array(
        'name'  => 'Anexos',
        'type'  => 'file_list',
        'id'    => 'anexos'
    ));

    $cmb->add_field(array(
        'name'  => 'Esforço',
        'type'  => 'number',
        'desc'  => 'Esforço em horas(hr)',
        'id'    => 'esforco',
        'attributes'    => array(
            'required'  => 'required'
        )
    ));
}

add_action('add_meta_boxes', 'extra_meta_boxes');
/**
 * Cria os metaboxes para checklist e membros,
 * criado separado por se tratar de um array
 */
function extra_meta_boxes(){
    $post_types = post_type_list();
    add_meta_box('membros', 'Membros', 'membros', $post_types);
    add_meta_box('checklist', 'Checklist', 'checklist', $post_types);
}

function membros(){}

function checklist($post_id){
//    $checklist = get_post_meta($post_id,'checklist', true);
//
//    $checklist = (is_null($checklist) || $checklist == '') ? $checklist : array();

    $arr = array(
        array(
            'check' => true,
            'desc'  => 'lorem'
        ),
        array(
            'check' => true,
            'desc'  => 'lorem'
        ),
        array(
            'check' => true,
            'desc'  => 'lorem'
        )
    );

    if(!empty($arr)) {
        ?>
        <table class="form-wrap">
        <?php
        foreach ($arr as $key => $value) {
            ?>
                <tr id="<?php echo $key; ?>">
                    <th scope="row">
                        <label for="">
                            <input type="checkbox" <?php echo ($value['check']) ? 'checked' : ''; ?>>
                        </label>
                    </th>
                    <td>
                        <input type="text" value="<?php echo $value['desc']; ?>" width="80%">
                    </td>
                </tr>
            <?php
        }
        ?>
        </table>
        <?php
    }
}
