<?php
/**
 * Pagina modificadora da WP API v2 do WordPress
 * para mais informações acesse a documentação oficial da API
 *
 * @author Vitor Hugo R Merencio
 * @api WP Rest API v2
 *
 */

/**
 * Recupera o custom post meta para usar na REST API
 *
 * @param  array $object Detalhes do post atual.
 * @param  string $field_name Nome do campo.
 * @param  WP_REST_Request $request Requisição atual.
 *
 * @return int
 */

function prefix_get_number_custom_meta($object, $field_name, $request){
  return (int) get_post_meta($object['id'], $field_name, true);
}

/**
  * Recupera o custom post meta para usar na REST API
  *
  * @param  array $object Detalhes do post atual.
  * @param  string $field_name Nome do campo.
  * @param  WP_REST_Request $request Requisição atual.
  *
  * @return string
 */

function prefix_get_text_custom_meta($object, $field_name, $request){
  return get_post_meta($object['id'], $field_name, true);
}

/**
 * Registra um valor numerico do tipo inteiro (int) no custom post meta
 *
 * @param  int $value Valor que será salvo.
 * @param  array $object Detalhes do post atual.
 * @param  string $field_name Nome do campo.
 *
 * @return mixed
 */

function prefix_update_number_custom_meta($value, $object, $field_name){
  if( !$value || ! is_numeric( $value )){
    return;
  }

  return update_post_meta( $object->ID, $field_name, (int) $value );
}

/**
 * Registra um valor do tipo textual (string) no custom post meta
 *
 * @param  string $value Valor que será salvo.
 * @param  array $object Detalhes do post atual.
 * @param  string $field_name Nome do campo.
 *
 * @return mixed
 */

function prefix_update_text_custom_meta($value, $object, $field_name){
  if( !$value || ! is_string( $value )){
    return;
  }

  return update_post_meta( $object->ID, $field_name, (string) $value );
}

/**
 * Função que recupera, atualiza e filtra os custom meta com a REST API
 */

function prefix_register_custom_meta(){
  $post_types = post_type_list();

  // Registrar o esforço
  register_rest_field(
    $post_types,
    'esforco',
    array(
      'get_callback'    => 'prefix_get_number_custom_meta',
      'update_callback' => 'prefix_update_number_custom_meta',
      'schema'          => null
    )
  );

  // Registra o esforço realizado
	register_rest_field(
		$post_types,
		'esforco_realizado',
		array(
			'get_callback'    => 'prefix_get_number_custom_meta',
			'update_callback' => 'prefix_update_number_custom_meta',
			'schema'          => null
		)
	);
}

add_action( 'rest_api_init', 'prefix_register_custom_meta' );
