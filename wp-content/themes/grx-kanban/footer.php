  <?php if(is_home() && is_user_logged_in() || is_page() && !is_page('Login') || is_archive() || is_single()  ) : ?>
    <footer class="twelve columns site-footer">
        <p>
          Vitor Hugo Rodrigues Merencio &copy; 2016
        </p>
    </footer>
  <?php endif; ?>
  <div id="overlay">
    <img src="<?php echo get_template_directory_uri().'/inc/img/ajax-loader.gif'; ?>" id="loader-image" alt="loader-gif" />
  </div>
</div>  <!-- Fechando a div do container -->
<?php wp_footer(); ?>
</body>
</html>
